<?php
    include "./../models/home.php";
    $home = new Home($connection);
    
    $id = "";
    $type = "";
    $value = "";
    if (isset($_POST['edit-data'])) {
        $getClient = $home->getClient('id', $_POST['id']);
        if ($getClient) {
            $data = $getClient->fetch_object();
            $id = $data->id;
            $type = $data->type;
            $value = $data->value;
        }
    }
    if (isset($_POST['delete']) || isset($_POST['add']) || isset($_POST['edit'])) {
        if (isset($_POST['delete'])) {
            $stmt = $home->delete('home_client', $_POST['id']);
        }
        if (isset($_POST['add'])) {
            $stmt = $home->addClient($_POST);
        }
        if (isset($_POST['edit'])) {
            $stmt = $home->editClient($_POST);
        }
        $alert = 'alert alert-success';
        $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
        if (!$stmt) {
            $alert = 'alert alert-danger';
            $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
        }
        echo "
        <div class='".$alert."'>
            ".$message."
        </div>
        ";
    }
    $getClientTitle = $home->getClient('type', 'title')->fetch_object();
    $idTitle = $getClientTitle->id;
    $valueTitle = $getClientTitle->value;
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $idTitle;?>">
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label" for="type">SELECT TYPE OF DATA</label>
                    <select type="text" name="type" class="form-control" id="type" readonly>
                        <option value="title">SECTION TITLE</option>
                    </select>
                </div>
                <div class="col-md-8">
                    <label class="control-label" for="value">VALUE OF DATA <small>(Max : 200 character)</small></label>
                    <input type="text" name="value" class="form-control" id="value" value="<?php echo $valueTitle;?>" maxlength="200" required>
                </div>
                <div class="col-md-1">
                    <label class="control-label" for="title"><hr></label>
                    <input type='submit' class='btn btn-info' name='edit' value='SAVE'>
                </div>
            </div>
        </div>
    </form>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $id;?>">
            <div class="form-group">
                <div class="col-md-3">
                    <label class="control-label" for="type">TYPE OF DATA</label>
                    <select type="text" name="type" class="form-control" id="type" readonly>
                        <option value="client">LOGO OF CLIENT</option>
                    </select>
                </div>
                <div class="col-md-8">
                    <label class="control-label" for="value">VALUE OF DATA <small>(Max : 200 character and must be a URL of logo)</small></label>
                    <input type="text" name="value" class="form-control" id="value" value="<?php echo $value;?>" maxlength="200" required>
                </div>
                <div class="col-md-1">
                    <label class="control-label" for="title"><hr></label>
                    <?php
                        if (isset($_POST['edit-data'])) {
                            echo "<input type='submit' class='btn btn-info' name='edit' value='SAVE'>";
                        } else {
                            echo "<input type='submit' class='btn btn-success' name='add' value='ADD'>";
                        }
                    ?>      
                </div>
            </div>
        </div>
    </form>
</div>
<div class="">
    <div class="col-lg-12">
        <div class = "table-resposive">
            <table id="myTable" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TYPE OF DATA</th>
                        <th>VALUE OF DATA</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $getClient = $home->getClient('type','client');
                if (!$getClient) {
                ?>
                    <tr>
                        <td colspan="7">Tidak Dapat Menampilkan Data</td>
                    </tr>
                <?php
                } else {
                    while($data = $getClient->fetch_object()){
                ?>
                    <tr>
                        <td align="center"><?php echo $no++ ?></td>
                        <td><?php echo $data->type; ?></td>
                        <td><?php echo $data->value; ?></td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                            <input type="submit" class="btn btn-warning btn-xs" name="edit-data" value="EDIT"/>
                            <input type="submit" class='btn btn-danger btn-xs' name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');"/>                            
                        </form>
                        </td>
                    </tr>
                <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
