<?php
    include "./../models/home.php";
    $home = new Home($connection);
    $id = "";
    $description = "";
    $title = "";
    $image = "";
    if (isset($_POST['edit-data'])) {
        $getService = $home->getService($_POST['id']);
        if ($getService) {
            $data = $getService->fetch_object();
            $id = $data->id;
            $description = $data->description;
            $title = $data->title;
            $image = $data->image;
        }
    }
?>
<div class="panel panel-default">
    <div class="panel-heading"><strong>Form Data</strong></div>
    <form method="post" enctype="multipart/form-data">
        <div class="panel-body">
            <input type="hidden" name="id" class="form-control" id="id" value="<?php echo $id;?>">
            <div class="form-group col-md-12">
                <div class="col-md-6">
                    <label class="control-label" for="title">TITLE <small>(Max : 50 character)</small></label>
                    <input type="text" name="title" class="form-control" id="title" value="<?php echo $title;?>" maxlength="50" required>
                </div>
                <div class="col-md-6">
                    <label class="control-label" for="image">SERVICE IMAGE URL <small>(Max : 100 character)</small></label>
                    <input type="text" name="image" class="form-control" id="image" value="<?php echo $image;?>" maxlength="100" required>
                </div>
            </div>
            <div class="form-group col-md-12">
                <div class="col-md-12">
                    <label class="control-label" for="description">SERVICE DESCRIPTION <small>(Max : 500 character)</small></label>
                    <textarea name="description" class="form-control" id="description" maxlength="500" required><?php echo $description;?></textarea>
                </div>
            </div>
            <!-- Button simpan -->
            <div class="col-md-12" style="text-align: right;">
                <div class="col-md-12">
                    <?php
                        if (isset($_POST['edit-data'])) {
                            echo "<input type='submit' class='btn btn-info' name='edit' value='EDIT & SAVE DATA'>";
                        } else {
                            echo "<input type='submit' class='btn btn-success' name='add' value='ADD NEW SERVICE'>";
                        }
                    ?>      
                </div>
            </div>
        </div>
    </form>
</div>
<div class="">
    <div class="col-lg-12">
    <?php
        if (isset($_POST['delete']) || isset($_POST['add']) || isset($_POST['edit'])) {
            if (isset($_POST['delete'])) {
                $stmt = $home->delete('home_service', $_POST['id']);
            }
            if (isset($_POST['add'])) {
                $stmt = $home->addService($_POST);
            }
            if (isset($_POST['edit'])) {
                $stmt = $home->editService($_POST);
            }
            $alert = 'alert alert-success';
            $message = '<strong>Success!</strong> Permintaan berhasil dilakukan.';
            if (!$stmt) {
                $alert = 'alert alert-danger';
                $message = '<strong>Fail!</strong> Gagal melakukan permintaan, silahkan coba kembali.';
            }
            echo "
            <div class='".$alert."'>
                ".$message."
            </div>
            ";
        }
    ?>
        <div class = "table-resposive">
            <table id="myTable" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>TITLE</th>
                        <th>DESCRIPTION</th>
                        <th>SERVICE IMAGE</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php
                $no = 1;
                $getService = $home->getService();
                if (!$getService) {
                ?>
                    <tr>
                        <td colspan="7">Tidak Dapat Menampilkan Data</td>
                    </tr>
                <?php
                } else {
                    while($data = $getService->fetch_object()){
            ?>
                    <tr>
                        <td align="center"><?php echo $no++ ?></td>
                        <td><?php echo $data->title; ?></td>
                        <td><?php echo $data->description; ?></td>
                        <td><?php echo "<a href='$data->image' target='_blank'/>".$data->image."</a>"; ?></td>
                        <td>
                        <form action="" method="post">
                            <input type="hidden" name="id" value="<?php echo $data->id; ?>"/>
                            <input type="submit" class="btn btn-warning btn-xs" name="edit-data" value="EDIT"/>
                            <input type="submit" class='btn btn-danger btn-xs' name="delete" value="DELETE" onclick="return confirm('Apakah anda yakin?');"/>                            
                        </form>
                        </td>
                    </tr>
                <?php
                    }
                }
                ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
