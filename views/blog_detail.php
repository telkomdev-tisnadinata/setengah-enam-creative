<div class="single-blog">
<?php
	$blogLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	if (!isset($_GET['id'])) {
		echo "<meta http-equiv = 'refresh' content = '0; url = ./blog' />"; 
		die;
	}
	$getBlog = $blog->getBlog($_GET['id']);
	if ($getBlog->num_rows == 0) {
		echo "<center>Blog post not found.</center>
		<br>
		<div class='clear'></div>";
	} else {
		$getBlog = $getBlog->fetch_object();
        $getComment = $blog->getBlogComment($getBlog->id);
	?>
		<div class="single-img">
			<img width="100%" src="<?php echo $getBlog->image; ?>" alt="" />
		</div>
		<div class="blog-desc">
			<h5><?php echo $getBlog->title; ?></h5>
			<div class="comment">
				<div class="comment-desc">
					<a class="no_link" onclick="blogIncrement('like', <?php echo $getBlog->id;?>)"><img src="images/heart.png" title="likes" alt=""/></a>
						<small id="like<?php echo $getBlog->id;?>"><?php echo $getBlog->like;?></small>&nbsp;&nbsp;&nbsp;&nbsp;
					<a class='no_link'>by <?php echo $getBlog->author; ?></a> / <a class='no_link'><?php echo $getComment->num_rows;?> comments</a> / 
					<a class='no_link'><?php echo date('d F Y', strtotime($getBlog->updatedAt)); ?></a>
				</div>
				<div class="clear"></div>
			</div>
		</div>
		<p class="blog-text">
			<?php echo $getBlog->description; ?>
		</p>
		<div class="share-point">
			<div class="point">
				<h5>Share this post</h5>
			</div>
			<div class="social-list">
				<ul>
					<li>
						<a onclick="blogIncrement('facebook',<?php echo $getBlog->id;?>)" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $blogLink;?>" rel="noopener noreferrer" target="_blank">
							<img src="images/f.png" alt="">
							<p id="facebook<?php echo $getBlog->id;?>"><?php echo $getBlog->facebook;?></p>
							<div class="clear"></div>
						</a>
					</li>
					<li>
						<a onclick="blogIncrement('twitter',<?php echo $getBlog->id;?>)" href="https://twitter.com/share?url=<?php echo $blogLink;?>" rel="noopener noreferrer" target="_blank">
							<img src="images/t.png" alt="">
							<p id="twitter<?php echo $getBlog->id;?>"><?php echo $getBlog->twitter;?></p>
							<div class="clear"></div>
						</a>
					</li>
					<li class="last">
						<a onclick="blogIncrement('linkedin',<?php echo $getBlog->id;?>)" href="https://linkedin.com/sharing/share-offsite/?url=<?php echo $blogLink;?>" rel="noopener noreferrer" target="_blank">
							<img src="images/i.png" alt="">
							<p id="linkedin<?php echo $getBlog->id;?>"><?php echo $getBlog->linkedin;?></p>
							<div class="clear"></div>
						</a>
					</li>
				</ul>
			</div>
			<div class="clear"></div>
		</div>
		<div class="related-post">
			<h4>Related Posts</h4>
			<div class="clear"></div>
			<div class="gallery">
				<ul>
				<?php
					$no=0;
					$getRelatedPost = $blog->getBlogSearchCategory($getBlog->category);
					$limit = ($getRelatedPost->num_rows > 3) ? 3 : $getRelatedPost->num_rows;
					for($i=0 ; $i<$limit ; $i++) {
						$data = $getRelatedPost->fetch_object()
						?>
						<li>
							<a class="" href="./blog-detail/blog/<?php echo $data->id;?>">
								<img src="<?php echo $data->image;?>" alt="" />
								<p><?php echo $data->title;?></p>
							</a>
						</li>
						<?php
					}
				?>
					<div class="clear"></div>
				</ul>
			</div>
		</div>
		<div style='margin-bottom: 10px;'>
			<?php
				if (isset($_POST['submit'])) {
					$_POST['id_blog'] = $getBlog->id;
					$addComment = $blog->addBlogComment($_POST);
					if ($addComment) {
						echo "<label style='color: green;'>New comment has been added. Thanks for your comment.</label>";
					} else {
						echo "<label style='color: red;'>Cant add your email, try again later.</label>";
					}
				}
			?>                        
			<div class="clear"></div>
		</div>
		<div class="blog-comment">
			<?php
				$getComment = $blog->getBlogComment($getBlog->id);
			?>
			<h5><?php echo $getComment->num_rows; ?> Comments</h5>
			<div class="clear"></div>
			<ul class="list">
			<?php
				if ($getComment->num_rows === 0) {
					echo "<div style='text-align: center; padding: 1%;'>No comments.</div>";
				} else {
					while($data = $getComment->fetch_object()){
						echo "
						<li>
							<div class='preview'>
								<a class='no_link'>
									<img src='images/c3.jpg' alt='' />
								</a>
							</div>
							<div class='data'>
								<div class='title'>$data->name<a class='no_link'>, ".date('d F Y', strtotime($data->updatedAt))."</a>
								</div>
								<p>$data->comment</p>
							</div>
							<div class='clear'></div>
						</li>
						";
					}
				}
			?>
			</ul>
		</div>
		<h5 class="leave">Leave a Comment</h5>
		<div class="clear"></div>
		<form method="post" action="">
			<div class="to">
				<input type="text" class="text" placeholder="Name..." name="name" required>
				<input type="text" class="text" placeholder="Email" style="margin-left: 1px" name="email" required>
			</div>
			<div class="text1">
				<textarea placeholder="Comment..." name="comment" required></textarea>
			</div>
			<div>
				<input type="submit" value="Add a comment" class="submit no_link" name="submit"/>
				<div class="clear"></div>
			</div>
		</form>
	<?php
	}
?>
</div>
