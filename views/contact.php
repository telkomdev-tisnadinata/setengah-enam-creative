<div class="banner">
    <div class="wrap">
        <h2>Contact</h2>
        <div class="clear"></div>
    </div>
</div>
<div class="main">
    <div class="project-wrapper">
        <div class="mapouter">
            <div class="gmap_canvas">
                <iframe width="100%" height="400" id="gmap_canvas" src="<?php echo $setting->getSettingData('type', 'address_map')->value?>" title="Address By Map"></iframe><a href="https://www.embedgooglemap.net/blog/nordvpn-coupon-code/">nordvpn coupon</a>
            </div>
            <style>
                .mapouter{position:relative;text-align:right;height:400px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:400px;width:100%;}
            </style>
        </div>
        <div class="wrap">
            <div class="contact">
                <div class="cont span_2_of_contact">
                    <h5 class="leave">Send Us A Message</h5>
                    <div class="clear"></div>
                    <div>
                        <?php
                            if (isset($_POST['send'])) {
                                require_once('./phpmailer/mail.php');
                                $mail = new Mail();
                                $message = "<strong>Reply this message to ".$_POST['email']."<br><br>Message:</strong><br>".$_POST['message'];
                                $send = $mail->sendMessage("team.secreative@gmail.com", $_POST['nama'], 'Message From Contact Website', $_POST['subject'], $message);
                                if ($send->status) {
                                    echo "<label style='color: green;'>Thanks for contact us.<br>We will contact you by email.</label>";
                                } else {
                                    echo "<label style='color: red;'>Cant send your message.<br>Please try again later.</label>";
                                }
                            }
                        ?>                        
                    </div>
                    <form method="post" action="">
                        <div class="contact-to">
                            <input type="text" class="text" placeholder="Name..." name="nama" required>
                            <input type="text" class="text" placeholder="Email..." name="email" required>
                            <input type="text" class="text" placeholder="Subject..." name="subject" required>
                        </div>
                        <div class="text2">
                            <textarea placeholder="Message:" name="message" required></textarea>
                        </div>
                        <div>
                            <input type="submit" value="Send a Message" class="submit" name="send"/>
                        </div>
                    </form>
                </div>
                <div class="lsidebar span_1_of_about">
                    <h5 class="leave">Contact Info</h5>
                    <div class="clear"></div>
                    <div class="contact-list">
                        <ul>
                            <li><img src="images/address.png" alt="">
                                <p><?php echo $setting->getSettingData('type', 'address')->value; ?></p>
                                <div class="clear"></div>
                            </li>
                            <li><img src="images/phone.png" alt="">
                                <p>Phone: <?php echo $setting->getSettingData('type', 'phone')->value; ?></p>
                                <div class="clear"></div>
                            </li>
                            <li><img src="images/msg.png" alt="">
                                <p>Email: <a class='no_link'><?php echo $setting->getSettingData('type', 'email')->value; ?></a></p>
                                <div class="clear"></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </div>
</div>