<?php                
    $no = 0;
    $blogLeft = "";
    $blogRight = "";
    if (isset($_POST['search'])) {
        $getBlog = $blog->getBlogSearchKeywoard($_POST['search']);
        if ($getBlog->num_rows == 0) {
            $blogLeft = "<center>Cant find any post that contain keyword '".$_POST['search']."'</center>";
        }
    } else if (isset($_GET['category'])) {
        $getBlog = $blog->getBlogSearchCategory($_GET['category']);
        if ($getBlog->num_rows == 0) {
            $blogLeft = "<center>Cant find any post.</center>";
        }
    } else if (isset($_GET['archive'])) {
        $getBlog = $blog->getArchive(null, $_GET['archive']);
        if ($getBlog->num_rows == 0) {
            $blogLeft = "<center>Cant find any post.</center>";
        }
    } else {
        $p = (isset($_GET['p']) ? $_GET['p'] : 1);
        $getBlog = $blog->getBlogPage($p);
    }
    while($data = $getBlog->fetch_object()){
        $category = str_replace(' ','',$data->category);
        $description = "";
        $getComment = $blog->getBlogComment($data->id);
        if ($no & 1) {  //TO CHECK IS $no VALUE IS ODD OR NOT
            $blogRight .= "
            <div class='blog-img'>
                <a href='./blog-detail/blog/$data->id'>
                    <img src='$data->image' alt='' />
                    <div class='blog-desc'>
                        <h5>$data->title</h5>
                        <p>$description</p>
                        <div class='comment'>
                            <span class='icon'><img src='images/heart.png' title='likes' alt=''/>$data->like</span>
                            <div class='comment-desc'><a class='no_link'>by $data->author</a> / <a class='no_link'>$getComment->num_rows comments</a> / <a class='no_link'>".date('d F Y', strtotime($data->updatedAt))."</a></div>
                            <div class='clear'></div>
                        </div>
                    </div>
                </a>
            </div>";
        } else {
            $blogLeft .= "
            <div class='blog-img'>
                <a href='./blog-detail/blog/$data->id'>
                    <img src='$data->image' alt='' />
                    <div class='blog-desc'>
                        <h5>$data->title</h5>
                        <p>$description</p>
                        <div class='comment'>
                            <span class='icon'><img src='images/heart.png' title='likes' alt=''/>$data->like</span>
                            <div class='comment-desc'><a class='no_link'>by $data->author</a> / <a class='no_link'>$getComment->num_rows comments</a> / <a class='no_link'>".date('d F Y', strtotime($data->updatedAt))."</a></div>
                            <div class='clear'></div>
                        </div>
                    </div>
                </a>
            </div>";
        }
        $no++;
    }
?>
<div class="blog-left">
    <!-- <div class="blog-bg">
        <h4>YOU CAN CHANGE THIS ONCE</h4><br><span class="author">- Setengah Enam Creative</span>
        <div class="clear"></div>
    </div> -->
    <?php 
        echo $blogLeft;
    ?>
</div>
<div class="blog-right">
    <?php 
        echo $blogRight;
    ?>
</div>
<div class="clear"></div>
<ul class="dc_pagination dc_paginationA dc_paginationA06">
    <?php
        if (!isset($_POST['search']) && !isset($_GET['category']) && !isset($_GET['archive'])) {
            if (!isset($_GET['p']) || @$_GET['p'] == 1) {
                echo "<li><a href='./blog/p/1' class='previous'>Previous</a></li>";
            } else {
                echo "<li><a href='./blog/p/".($_GET['p']-1)."' class='previous'>Previous</a></li>";
            }
            $totalPage = $blog->getBlogTotalPage();
            for ($p=1 ; $p<=$totalPage ; $p++) {
                if (!isset($_GET['p']) && $p == 1) {
                    $current = "current";
                }else if (@$_GET['p'] == $p) {
                    $current = "current";
                } else {
                    $current = "";
                }
                echo "<li><a href='./blog/p/$p' class='".($current)."'>$p</a></li>";
            }
            if (!isset($_GET['p']) || @$_GET['p'] == $totalPage) {
                echo "<li><a href='./blog/p/$totalPage' class='next'>Next</a></li>";
            } else {
                echo "<li><a href='./blog/p/".($_GET['p']+1)."' class='next'>Next</a></li>";
            }
        }
    ?>
</ul>
