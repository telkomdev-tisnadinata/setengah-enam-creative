<?php
class Blog {
    private $mysqli;

    function __construct($conn){
        $this->mysqli = $conn;
    }
    public function getArchive($year = null, $month = null){
        $db = $this->mysqli->conn;
        if($year == null){
            $year = "YEAR(createdAt) = YEAR(CURRENT_DATE())";
        } else {
            $year = "YEAR(createdAt) = '$year'";
        }
        $sql = "SELECT * FROM tbl_blog WHERE $year";
        if($month != null){
            $sql .= " AND MONTH(createdAt) = $month";
        }
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getBlogCategory($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT id, category FROM tbl_blog";
        if($id != null){
            $sql .= " WHERE id = '$id'";
        }
        $sql .= " GROUP BY category ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getBlog($id = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_blog";
        if($id != null){
            $sql .= " WHERE id = '$id'";
        }
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getBlogSearchCategory($category = ''){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_blog WHERE category LIKE '$category'";
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getBlogSearchKeywoard($search = ''){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_blog WHERE title LIKE '%$search%' OR `description` LIKE '%$search%'";
        $sql .= " ORDER BY updatedAt DESC";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getBlogTotalPage(){
        $db = $this->mysqli->conn;
        $query = $this->getBlog();
        $totalPage = 0;
        if($query){
            $totalPage = $query->num_rows;
        }
        $totalPage = ceil($totalPage/5);
        return $totalPage;
    }
    public function getBlogPage($page = 1){
        $db = $this->mysqli->conn;
        $page = ($page-1)*5;
        $sql = "SELECT * FROM tbl_blog ORDER BY updatedAt DESC LIMIT $page, 5";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getBlogLimit($limit = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_blog ORDER BY createdAt DESC";
        if($limit != null){
            $sql .= " LIMIT 0, $limit";
        }
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getBlogPopular($limit = null){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_blog ORDER BY `like` DESC";
        if($limit != null){
            $sql .= " LIMIT 0, $limit";
        }
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function getBlogComment($id){
        $db = $this->mysqli->conn;
        $sql = "SELECT * FROM tbl_blog_comment WHERE id_blog = '$id' ORDER BY `updatedAt` DESC";
        if($id == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function delete($table, $id){
        $db = $this->mysqli->conn;
        $sql = "DELETE FROM tbl_$table WHERE id = '$id'";
        if($id == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function addBlog($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO `tbl_blog` (`category`, `author`, `title`, `description`, `image`, `like`) ";
        $sql .= "VALUES('".$data['category']."','".$data['author']."','".$data['title']."','".$data['description']."','".$data['image']."',0);";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
    public function editBlog($data){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_blog SET category = '".$data['category']."',author = '".$data['author']."' , title = '".$data['title']."', description = '".$data['description']."', ";
        $sql .= "image = '".$data['image']."' WHERE id = '".$data['id']."'";
        if($data['id'] == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function blogLike($id){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_blog SET `like` = `like`+1 WHERE id = '$id'";
        if($id == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function blogShare($socialMedia, $id){
        $db = $this->mysqli->conn;
        $sql = "UPDATE tbl_blog SET `$socialMedia` = `$socialMedia`+1 WHERE id = '$id'";
        if($id == null){
            $query = false;
        } else {
            $query = $db->query($sql) or ($db->error);
        }
        return $query;
    }
    public function addBlogComment($data){
        $db = $this->mysqli->conn;
        $sql = "INSERT INTO `tbl_blog_comment` (`id_blog`, `name`, `email`, `comment`) ";
        $sql .= "VALUES('".$data['id_blog']."','".$data['name']."','".$data['email']."','".$data['comment']."');";
        $query = $db->query($sql) or ($db->error);
        return $query;
    }
}
?>